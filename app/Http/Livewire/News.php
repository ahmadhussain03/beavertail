<?php

namespace App\Http\Livewire;

use App\News as AppNews;
use Livewire\Component;
use Livewire\WithPagination;

class News extends Component
{

    use WithPagination;

    public function previousPage()
    {
        $this->page = $this->page - 1;
        $this->emit('scrollTop');
    }

    public function nextPage()
    {
        $this->page = $this->page + 1;
        $this->emit('scrollTop');
    }

    public function gotoPage($page)
    {
        $this->page = $page;
        $this->emit('scrollTop');
    }

    public function resetPage()
    {
        $this->page = 1;
        $this->emit('scrollTop');
    }

    public function render()
    {
        return view('livewire.news', [
            'news' => AppNews::with('tags')->with('user')->orderBy('created_at', 'desc')->paginate(4),
            'popular' => visits('App\News')->top(5)
        ]);
    }
}
