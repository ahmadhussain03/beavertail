<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Forum as AppForum;
use App\ForumComment;
use Livewire\WithPagination;

class Forum extends Component
{
    use WithPagination;

    public $forum, $comment;
    public $showForm = false;

    public function mount($id)
    {
        $forum = AppForum::with(['category', 'user'])->where('id', $id)->firstOrFail();
        $forum->vzt()->increment();
        $this->forum = $forum;
    }

    public function toggleForm()
    {
        $this->showForm = !$this->showForm;
    }

    public function createComment()
    {
        $validatedData = $this->validate([
            'comment' => 'required|min:2|string|max:255'
        ]);

        $validatedData['user_id'] = auth()->id();
        $validatedData['forum_id'] = $this->forum->id;

        ForumComment::create($validatedData);
        $this->showForm = false;
    }

    public function render()
    {
        return view('livewire.forum', [
            'comments' => ForumComment::where('forum_id', $this->forum->id)->orderBy('created_at', 'desc')->paginate(5)
        ]);
    }
}
