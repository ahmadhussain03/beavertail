<?php

namespace App\Http\Livewire;

use App\News;
use Livewire\Component;
use Livewire\WithPagination;

class UserNews extends Component
{
    use WithPagination;

    public function deleteNews($id)
    {
        $this->emit('postAdded', $id);
        $news = News::where('id', $id)->where('user_id', auth()->id())->firstOrFail();
        $news->delete();
    }

    public function render()
    {
        $articles = News::where('user_id', auth()->id())->paginate(10);
        return view('livewire.user-news', ['articles' => $articles]);
    }
}
