<?php

namespace App\Http\Livewire;

use App\News;
use App\NewsComment;
use Livewire\Component;
use Livewire\WithPagination;

class VerifyComment extends Component
{
    use WithPagination;

    public $showForm = false;
    public $comment, $url, $news;

    public function mount(News $news)
    {
        $this->news = $news;
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'comment' => 'required|min:2|string|max:255',
            'url' => 'required|active_url',
        ]);
    }

    public function toggleForm()
    {
        $this->showForm = !$this->showForm;
    }

    public function createComment()
    {
        $validatedData = $this->validate([
            'comment' => 'required|min:2|string|max:255',
            'url' => 'required|active_url',
        ]);

        $validatedData['is_verified'] = true;
        $validatedData['user_id'] = auth()->id();
        $validatedData['news_id'] = $this->news->id;

        NewsComment::create($validatedData);
        $this->showForm = false;
    }

    public function render()
    {
        if(auth()->user()->is_admin){
            $comments = NewsComment::withTrashed()->with('user')->with('news')->where('is_verified', true)->where('news_id', $this->news->id)->orderBy('created_at', 'desc')->paginate(5);
        } else {
            $comments = NewsComment::with('user')->with('news')->where('is_verified', true)->where('news_id', $this->news->id)->orderBy('created_at', 'desc')->paginate(5);
        }

        return view('livewire.verify-comment', [
            'comments' => $comments
        ]);
    }
}
