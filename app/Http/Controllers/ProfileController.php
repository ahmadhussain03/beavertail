<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $articles = News::where('user_id', auth()->id())->paginate(10);
        return view('profile.index', compact('articles'));
    }

    public function image(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:png,jpg,bmp,jpeg'
        ]);

        $user = auth()->user();

        $imageName = time() . $request->image->getClientOriginalName();
        $imagePath = 'images/profile/'. $imageName;
        if($request->image->move('images/profile', $imageName)){
            if(file_exists($user->image_path)){
                unlink($user->image_path);
            }

            $user->image_path = $imagePath;
            $user->image_url = asset($imagePath);
            $user->save();
        }

        session()->flash('success', 'Profile Image Updated Successfully!');
        return back();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . auth()->id()],
            'password' => ['nullable', 'string', 'min:8'],
        ]);

        $user = auth()->user();
        $user->name = $request->name;
        $user->email = $request->email;

        if($request->password){
            $user->password = bcrypt($request->password);
        }

        $user->save();

        session()->flash('success', 'Profile Updated Successfully');
        return back();
    }
}
