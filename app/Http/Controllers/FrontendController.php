<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
}
