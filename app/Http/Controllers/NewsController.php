<?php

namespace App\Http\Controllers;

use App\Tag;
use App\News;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function create()
    {
        if(auth()->user()->status == 0){
            return back();
        }
        return view('news.create');
    }

    public function store(Request $request)
    {
        if(auth()->user()->status == 0){
            return back();
        }
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'url' => 'required|active_url',
            'synopsis' => 'required|string',
            'date' => 'required|date',
            'location' => 'required|string',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
            'tags' => 'required|array',
            'tags.*' => 'required|string',
            'image' => 'sometimes|mimes:png,jpg,bmp,jpeg'
        ]);

        $news = News::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'url' => $request->url,
            'synopsis' => $request->synopsis,
            'date' => $request->date,
            'url' => $request->url,
            'latitude' => $request->lat,
            'longitude' => $request->long,
            'location' => $request->location,
            'user_id' => auth()->id()
        ]);

        if($request->has('image')){
            $imageName = time() . $request->image->getClientOriginalName();
            $imagePath = 'images/news/' . $imageName;
            if($request->image->move('images/news', $imageName)){
                $news->image_url = asset($imagePath);
                $news->image_path = $imagePath;
                $news->save();
            }
        }

        foreach($request->tags as $tag){
            Tag::create([
                'name' => $tag,
                'news_id' => $news->id
            ]);
        }

        session()->flash('success', 'News Posted Successfully');
        return redirect()->route('welcome');

    }

    public function show($slug, $date)
    {
        $news = News::with('user')->with('tags')->where('slug', $slug)->where('date', $date)->firstOrFail();
        $news->vzt()->increment();
        return view('news.show', ['news' => $news]);
    }
}
