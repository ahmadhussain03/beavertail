<?php

namespace App\Http\Controllers;

use App\Forum;
use App\ForumCategory;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function index()
    {
        $categories = ForumCategory::with('forums')->get();
        return view('forum.index', ['categories' => $categories]);
    }

    public function show($id)
    {
        return view('forum.show', ['id' => $id]);
    }
}
