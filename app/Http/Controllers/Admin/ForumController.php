<?php

namespace App\Http\Controllers\Admin;

use App\Forum;
use App\ForumCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;


class ForumController extends Controller
{
    public function index()
    {
        return view('admin.forum.index');
    }

    public function create()
    {
        $categories = ForumCategory::all();
        return view('admin.forum.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:255',
            'body' => 'required|string',
            'category' => 'required|integer|min:1'
        ]);

        $category = ForumCategory::findOrFail($request->category);

        Forum::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'body' => $request->body,
            'forum_category_id' => $category->id,
            'user_id' => auth()->id()
        ]);

        return redirect()->route('admin.forum.index');
    }

    public function edit($id)
    {
        $forum = Forum::findOrFail($id);
        $categories = ForumCategory::all();
        return view('admin.forum.edit', ['categories' => $categories, 'forum' => $forum]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:255',
            'body' => 'required|string',
            'category' => 'required|integer|min:1'
        ]);

        $forum = Forum::findOrFail($id);
        $category = ForumCategory::findOrFail($request->category);

        $forum->title = $request->title;
        $forum->slug = Str::slug($request->title);
        $forum->body = $request->body;
        $forum->forum_category_id = $category->id;
        $forum->save();

        return redirect()->route('admin.forum.index');
    }

    public function destroy($id)
    {
        $forum = Forum::findOrFail($id);
        $forum->delete();
        return back();
    }

    public function list()
    {
        $forums = Forum::query()->with('category')->orderBy('created_at', 'desc');

        return Datatables::of($forums)
        ->addColumn('action', function(Forum $forum){})
        ->make(true);
    }
}
