<?php

namespace App\Http\Controllers\Admin;

use App\News;
use App\NewsComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;


class NewsController extends Controller
{
    public function index()
    {
        return view('admin.news.index');
    }

    public function comments()
    {
        return view('admin.news.comment');
    }

    public function deletedComments()
    {
        return view('admin.news.delete-comments');
    }

    public function destroy($id)
    {
        $news = News::findOrFail($id);
        if(file_exists($news->image_path)){
            unlink($news->image_path);
        }
        $news->delete();

        return back();
    }

    public function destroyComment($id)
    {
        $comment = NewsComment::findOrFail($id);
        $comment->delete();

        return back();
    }

    public function restoreComment($id)
    {
        $comment = NewsComment::withTrashed()->where('id', $id)->firstOrFail();
        $comment->restore();

        return back();
    }

    public function forceDestroyComment($id)
    {
        $comment = NewsComment::withTrashed()->where('id', $id)->firstOrFail();
        $comment->forceDelete();

        return back();
    }

    public function list()
    {
        $news = News::query()->with('user')->orderBy('created_at', 'desc');

        return Datatables::of($news)
        ->addColumn('image', function(News $obj){
            return '<img src="' . $obj->image_url . '" class="img-thumbnail" width="50" />';
        })
        ->addColumn('action', function(News $obj){})
        ->rawColumns(['image'])
        ->make(true);
    }

    public function commentList()
    {
        $comments = NewsComment::query()->with('user')->with('news')->orderBy('created_at', 'desc');

        return Datatables::of($comments)
        ->addColumn('action', function(NewsComment $comment){})
        ->make(true);
    }

    public function deletedCommentList()
    {
        $comments = NewsComment::query()->onlyTrashed()->with('user')->with('news')->orderBy('created_at', 'desc');

        return Datatables::of($comments)
        ->addColumn('action', function(NewsComment $comment){})
        ->make(true);
    }
}
