<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.user.index');
    }

    public function new()
    {
        return view('admin.user.new');
    }

    public function ban($id)
    {
        if(auth()->id() != $id){
            $user = User::findOrFail($id);
            $user->is_banned = true;
            $user->save();
        }

        return back();
    }

    public function unban($id)
    {
        $user = User::findOrFail($id);
        $user->is_banned = false;
        $user->save();

        return back();
    }

    public function approve($id)
    {
        $user = User::findOrFail($id);
        $user->status = 1;
        $user->save();

        return back();
    }

    public function destroy($id)
    {
        if(auth()->id() != $id){
            $user = User::findOrFail($id);
            if(file_exists($user->image_path)){
                unlink($user->image_path);
            }
            $user->delete();
        }

        return back();
    }

    public function list()
    {
        $users = User::query()->where('status', 1);

        return Datatables::of($users)
        ->addColumn('action', function($user){

        })
        ->make(true);
    }

    public function newList()
    {
        $users = User::query()->where('status', 0)->where('is_banned', false);

        return Datatables::of($users)
        ->addColumn('action', function($user){

        })
        ->make(true);
    }
}
