<?php

namespace App\Http\Controllers\Admin;

use App\ForumCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ForumCategoryController extends Controller
{
    public function index()
    {
        return view('admin.category.index');
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:255',
            'description' => 'required|string'
        ]);

        ForumCategory::create([
            'name' => $request->name,
            'description' => $request->description,
            'is_comment_allowed' => $request->comment_allowed ? true : false
        ]);

        return redirect()->route('admin.category.index');
    }

    public function edit($id)
    {
        $category = ForumCategory::findOrFail($id);
        return view('admin.category.edit', ['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:255',
            'description' => 'required|string'
        ]);

        $category = ForumCategory::findOrFail($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->is_comment_allowed = $request->comment_allowed ? true : false;
        $category->save();

        return redirect()->route('admin.category.index');
    }

    public function destroy($id)
    {
        $category = ForumCategory::findOrFail($id);
        $category->delete();
        return back();
    }

    public function list()
    {
        $categories = ForumCategory::query()->orderBy('created_at', 'desc');

        return Datatables::of($categories)
        ->addColumn('action', function(ForumCategory $comment){})
        ->make(true);
    }
}
