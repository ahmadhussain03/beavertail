<?php

namespace App\Http\Controllers;

use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PinsController extends Controller
{
    public function index()
    {
        $news = News::where('created_at', '>=', Carbon::now()->subDay())->get();
        return response()->json(['news' => $news]);
    }
}
