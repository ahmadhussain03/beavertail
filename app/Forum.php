<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $guarded = [];

    public function vzt()
    {
        return visits($this);
    }

    public function visits()
    {
        return visits($this)->relation();
    }

    public function category()
    {
        return $this->belongsTo('App\ForumCategory', 'forum_category_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\ForumComment');
    }
}
