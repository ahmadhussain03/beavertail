<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->truncate();

        User::create([
            'email' => 'ahmadhussain03@hotmail.com',
            'username' => 'ahmad',
            'password' => bcrypt('admin123'),
            'name' => 'Ahmad Hussain',
            'status' => 1,
            'is_admin' => true
        ]);

        User::create([
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'password' => bcrypt('PA8%y3GzCdg5'),
            'name' => 'Administrator',
            'status' => 1,
            'is_admin' => true
        ]);
    }
}
