<?php

use App\News;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        News::query()->truncate();

        News::create([
            'title' => 'Daniel Prude: New York police used "spit hood" on man who died of asphyxiation',
            'slug' => Str::slug('Daniel Prude: New York police used "spit hood" on man who died of asphyxiation'),
            'url' => 'https://www.bbc.com/news/world-us-canada-54007884',
            'synopsis' => 'An unarmed black man died in New York state after he was hooded by police and held face down to the road for two minutes, body camera footage shows.',
            'date' => '09/03/2020',
            'Location' => 'New York',
            'longitude' => -73.935242,
            'latitude' => 40.730610,
            'user_id' => 1,
            'image_url' => 'http://beavertail.test/images/news/black-man.png',
        ]);

        News::create([
            'title' => 'Kafeel Khan: National threat or symbol of repression?',
            'slug' => Str::slug('Kafeel Khan: National threat or symbol of repression?'),
            'url' => 'https://www.bbc.com/news/world-us-canada-54007884',
            'synopsis' => 'For more than 200 days, a young, jailed Indian doctor faced charges under a national security law that allows the authorities to detain people if they feel they are a threat to national security.',
            'date' => '09/03/2020',
            'Location' => 'Delhi, India',
            'longitude' => 77.22897,
            'latitude' => 28.65381,
            'user_id' => 1,
            'image_url' => 'http://beavertail.test/images/news/doctor.jpg',
        ]);
    }
}
