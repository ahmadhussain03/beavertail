<?php

use App\News as Newses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FrontendController@index')->name('welcome');

// Get Pins
Route::get('pins/current', 'PinsController@index')->name('pins.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('news/{slug}/{date}', 'NewsController@show')->name('news.show');
Route::get('forums', 'ForumController@index')->name('forum.index');
Route::get('forum/{id}', 'ForumController@show')->name('forum.show');

Route::group(['middleware' => 'auth'], function(){
    // Profile Update Routes
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::post('profile/image/update', 'ProfileController@image')->name('profile.image.update');
    Route::post('profile/update', 'ProfileController@update')->name('profile.update');

    // News Routes
    Route::get('news/create', 'NewsController@create')->name('news.create');
    Route::post('news/store', 'NewsController@store')->name('news.store');

    Route::group(['middleware' => 'admin', 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
        Route::get('dashboard', 'AdminController@index')->name('admin.index');

        // User Controller
        Route::get('users/all', 'UserController@index')->name('admin.user.index');
        Route::get('users/new', 'UserController@new')->name('admin.user.new');
        Route::get('users/all/list', 'UserController@list')->name('admin.user.list');
        Route::get('users/new/list', 'UserController@newList')->name('admin.user.new.list');
        Route::get('user/{id}/ban', 'UserController@ban')->name('admin.user.ban');
        Route::get('user/{id}/unban', 'UserController@unban')->name('admin.user.unban');
        Route::get('user/{id}/approve', 'UserController@approve')->name('admin.user.approve');
        Route::get('user/{id}/delete', 'UserController@destroy')->name('admin.user.destroy');

        // News Controller
        Route::get('news', 'NewsController@index')->name('admin.news.index');
        Route::get('news/comments', 'NewsController@comments')->name('admin.comment.index');
        Route::get('news/list', 'NewsController@list')->name('admin.news.list');
        Route::get('news/comments/list', 'NewsController@commentList')->name('admin.comment.list');
        Route::get('news/{id}/delete', 'NewsController@destroy')->name('admin.news.destroy');
        Route::get('comment/{id}/delete', 'NewsController@destroyComment')->name('admin.comment.destroy');
        Route::get('comment/{id}/restore', 'NewsController@restoreComment')->name('admin.comment.restore');
        Route::get('comment/{id}/delete/force', 'NewsController@forceDestroyComment')->name('admin.comment.force.destroy');
        Route::get('news/comment/deleted', 'NewsController@deletedComments')->name('admin.comment.delete.index');
        Route::get('news/comments/deleted/list', 'NewsController@deletedCommentList')->name('admin.comment.delete.list');


        // Forum Category Routes
        Route::get('categories', 'ForumCategoryController@index')->name('admin.category.index');
        Route::get('category/create', 'ForumCategoryController@create')->name('admin.category.create');
        Route::post('category/store', 'ForumCategoryController@store')->name('admin.category.store');
        Route::get('category/{id}/edit', 'ForumCategoryController@edit')->name('admin.category.edit');
        Route::post('category/{id}/update', 'ForumCategoryController@update')->name('admin.category.update');
        Route::get('category/{id}/delete', 'ForumCategoryController@destroy')->name('admin.category.destroy');
        Route::get('category/list', 'ForumCategoryController@list')->name('admin.category.list');

        // Forum Routes
        Route::get('forums', 'ForumController@index')->name('admin.forum.index');
        Route::get('forum/create', 'ForumController@create')->name('admin.forum.create');
        Route::post('forum/store', 'ForumController@store')->name('admin.forum.store');
        Route::get('forum/{id}/edit', 'ForumController@edit')->name('admin.forum.edit');
        Route::post('forum/{id}/update', 'ForumController@update')->name('admin.forum.update');
        Route::get('forum/{id}/delete', 'ForumController@destroy')->name('admin.forum.destroy');
        Route::get('forum/list', 'ForumController@list')->name('admin.forum.list');
    });

});
