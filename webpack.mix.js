const mix = require('laravel-mix');

const purgecss = require('@fullhuman/postcss-purgecss')({
    // Specify the paths to all of the template files in your project
    content: [
        './resources/**/*.blade.php',
        './resources/**/*.vue'
    ],

    // Include any special characters you're using in this regular expression
    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
})

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/sass/app.css', 'public/css', [
        require('tailwindcss'),
        ...mix.inProduction()
            ? [purgecss]
            : [],
        require('autoprefixer')
    ])
    .options({
        processCssUrls: false
});

mix.js('resources/js/create-news.js', 'public/js')
mix.js('resources/js/profile-settings.js', 'public/js')
mix.js('resources/js/show-pins.js', 'public/js')

mix.webpackConfig({
    resolve: {
        alias: {
            '@components': path.resolve(__dirname, "resources/js/components")
        }
    }
});

mix.disableNotifications();
