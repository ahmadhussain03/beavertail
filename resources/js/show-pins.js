
let map = null;
let markers = [];

function flyToLocation(coordinates){
    map.flyTo({
        center: coordinates,
        essential: true
    })
}


function showPins(news){
    // Create Popup
    let popup = new mapboxgl.Popup({ closeOnClick: false, offset: 15 })
    .setHTML(`<div class="flex flex-col justify-center items-center"><img src="${news.image_url}" width="100%" height="100"><h2 style="text-align: center">${news.title}</h2></div>`);

    let center = [news.longitude, news.latitude];

    console.log(center);

    // create the marker
    let marker = new mapboxgl.Marker()
    .setLngLat(center)
     .setPopup(popup) // sets a popup on this marker
     .addTo(map);

     markers.push(marker);
}

$(document).ready(() => {

    // Initialize Maps
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWhtYWRodXNzYWluIiwiYSI6ImNrZGlxZzFzbzA3N3MzMHJvMjh2anVha3cifQ.CPKuVXC2G3Dic88nJRijbQ';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        // zoom: 13
    });

    map.addControl(new mapboxgl.NavigationControl());



    // Get User Location
    $.getJSON('https://geolocation-db.com/json/')
        .done (function(location) {
            coordinates = [location.longitude, location.latitude]
            flyToLocation(coordinates)
    });

    axios.get('/pins/current').then(res => {
         let news = res.data.news;
         news.forEach(n => {
             showPins(n);
         })
    }).catch(err => {
        console.error(err);
    })

});
