
const MapBoxAccessToken = 'pk.eyJ1IjoiYWhtYWRodXNzYWluIiwiYSI6ImNrZTVlemdoMDEyOGwycm1zdjZpcmNjYTcifQ.c6fY2tGCyssRuoyGCIuAJw'
let featurePlaces = [];
let coordinates = [0, 0];
let tags = [];
let defaultImage = "";
let previewImage = "";
let map = null;
let marker = null;
let popup = null;

function flyToLocation(coordinates){
    map.flyTo({
        center: coordinates,
        essential: true
    })
}

function createMarker(center){

    if(marker){
        marker.remove();
    }
    let title = $("#titleInput").val()
    // Create Popup
    popup = new mapboxgl.Popup({ closeOnClick: false, offset: 15 })
        .setHTML(`<div class="flex flex-col justify-center items-center"><img src="${previewImage}" width="100%" height="100"><h2 style="text-align: center">${title}</h2></div>`);

    // create the marker
    marker = new mapboxgl.Marker()
        .setLngLat(center)
        .setPopup(popup) // sets a popup on this marker
        .addTo(map);
}

$(document).ready(() => {

    // Initialize Maps
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWhtYWRodXNzYWluIiwiYSI6ImNrZGlxZzFzbzA3N3MzMHJvMjh2anVha3cifQ.CPKuVXC2G3Dic88nJRijbQ';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        // zoom: 13
    });

    map.addControl(new mapboxgl.NavigationControl());

    $("#titleInput").keyup((e) => {
        if(popup)
            popup.setHTML(`<img src="${previewImage}" width="100%" height="100"><h2>${e.target.value}</h2>`);
    })

    // Listen for tag input
    $("#tagsInput").keyup((e) => {
        if(e.which == 32){
            if(e.target.value.trim().length > 0){
                tags.push(e.target.value.trim());
                $("#tagsInput").val("");
                createTags();
            }
        }
    });

    // Get User Location
    $.getJSON('https://geolocation-db.com/json/')
        .done (function(location) {
            coordinates = [location.longitude, location.latitude]
            flyToLocation(coordinates)
    });

    // Search Places
    let search = debounce((e) => {
        serachPlaces(encodeURIComponent(e.target.value));
    }, 500);

    // Select Searched Place
    $("#locations").on("click", "a" , function(e) {
        e.preventDefault();
        let index = e.target.dataset.index;
        let place = featurePlaces[index];
        $("#locationField").val(place.place_name);
        $("#locationInput").val(place.place_name);
        $("#long").val(place.geometry.coordinates[0]);
        $("#lat").val(place.geometry.coordinates[1]);
        flyToLocation(place.geometry.coordinates);
        createMarker(place.geometry.coordinates);
        $("#locations").hide();
    });

    // Listen to Remove the Tags
    $("#tags").on("click", ".remove-tag", function(e){
        e.preventDefault();
        let index = e.target.dataset.index;
        tags.splice(index, 1);
        createTags();
    });

    $("#locationField").keyup(search);

    // Read Image Show
    defaultImage = $("#imagePreview").attr('src');
    previewImage = defaultImage;
    $("#image").change((e) => {
        let files = e.target.files;
        if(files && files[0]){
            let reader = new FileReader();
            reader.onload = (e) => {
                $("#removeImage").show();
                $("#imagePreview").attr("src", e.target.result)
                previewImage = e.target.result;
                if(popup){
                    let title = $("#titleInput").val()
                    popup.setHTML(`<img src="${previewImage}" width="100%" height="100"><h2>${title}</h2>`);
                }
            }
            reader.readAsDataURL(files[0])
        }
    });

    $("#selectFile").click((e) => {
        e.preventDefault();
        document.getElementById("image").click();
    })

    $("#removeImage").click((e) => {
        e.preventDefault();
        $("#imagePreview").attr("src", defaultImage)
        $("#image").val("");
        $("#removeImage").hide();
        previewImage = defaultImage;

        if(popup){
            let title = $("#titleInput").val()
            popup.setHTML(`<img src="${previewImage}" width="100%" height="100"><h2>${title}</h2>`);

        }
    })

});


const serachPlaces = (searchTerm) => {
    if(searchTerm.length <= 0)
        return;
    $('.spinner').show()
    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${searchTerm}.json?access_token=${MapBoxAccessToken}`
    axios.get(url).then(res => {
        $('.spinner').hide()

        let features = res.data.features;
        featurePlaces = features;
        createLocationList(features);
        $("#locations").show();
    }).catch(err => {
        $('.spinner').hide()
        console.error(err)
    })
}

const createTags = () => {
    $("#tags").children().remove();
    tags.forEach((tag, index) => {
       let t = createTag(tag, index)
       $("#tags").append(t);
    });
};

const createTag = (tag, index) => {
    let template = `
        <span class="remove-tag mr-1 flex items-center rounded-md bg-indigo-200 px-2 py-2 text-indigo-800">
                <svg data-index="${index}" class="w-6 h-6 cursor-pointer" viewBox="0 0 20 20" fill="currentColor" class="minus-circle w-6 h-6">
                    <path data-index="${index}" fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z" clip-rule="evenodd"></path>
                </svg>
                <span class="ml-1">${tag}</span>
        </span>
        <input type="hidden" name="tags[]" value="${tag}">
    `;

    return template;
}

const createLocationItem = (item, index) => {
    let template = `
    <li class="py-2 px-1 border-b border-gray-400 hover:bg-white location-item" data-index="${index}">
        <a href="" class="flex items-center location-item" data-index="${index}">
            <svg data-index="${index}" viewBox="0 0 20 20" fill="currentColor" class="location-marker w-4 h-4"><path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd"></path></svg>
            <span data-index="${index}" class="ml-1">${item.place_name}</span>
        </a>
    </li>
    `;

    return template
}

const createLocationList = (items) => {
    $("#locations").children().remove();
    const ul = document.createElement('ul')
    ul.setAttribute('id', 'location-list')
    $("#locations").append(ul);
    items.forEach((item, index) => {
        let template = createLocationItem(item, index);
        $("#location-list").append(template);
    })
}

