
$(document).ready(() => {
    let defaultImage = $("#image").attr('src');

    $("#selectFile").click((e) => {
        e.preventDefault();
        document.getElementById("fileInput").click();
    })

    $("#removeImage").click(e => {
        e.preventDefault();
        $("#fileInput").val("");
        $("#image").attr("src", defaultImage);
        $("#removeImage").hide();
        $("#updateImage").hide();
        $("#selectFile").show();
    })


    $("#fileInput").change((e) => {
        let files = e.target.files;
        if(files && files[0]){
            let reader = new FileReader();
            reader.onload = (e) => {
                $("#selectFile").hide();
                $("#removeImage").show();
                $("#updateImage").show();
                $("#image").attr("src", e.target.result)
            }
            reader.readAsDataURL(files[0])
        }
    });

    $("#updateImage").click(e => {
        e.preventDefault();

        $("#imageForm").submit();
    })
})
