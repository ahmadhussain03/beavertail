@extends('layouts.app')

@section('styles')
    @livewireStyles
@endsection

@section('content')
    <div class="container mx-auto my-2">
        @if(session('success'))
            <div class="mt-3 bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
                <div class="flex">
                    <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                    <div>
                        <p class="font-bold">{{ session('success') }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="mt-2 flex">
            <div class="p-8 w-full" id="recentPosts">
                <h2 class="text-4xl border-indigo-400 border-b-4 pb-3 mb-4 uppercase">Forum</h2>
                <div class="flex flex-wrap justify-between items-center">
                    @foreach($categories as $category)
                        @if($category->forums->count() > 0)
                            <div class="w-full">
                                <h3 class="text-3xl py-1">{{ $category->name }}</h3>
                                <div class="flex">
                                    @foreach($category->forums as $forum)
                                        <div class="w-full border border-gray-500 rounded-md shadow-sm p-3">
                                            <a href="{{ route('forum.show', ['id' => $forum->id]) }}"><p class="text-xl">{{ $forum->title }}</p></a>
                                            <p class="text-sm py-1 text-gray-700"><span class="mr-3">{{ $forum->vzt()->count() }} Views</span> <span class="mr-3">0 Comments</span> <span class="mr-3">by {{ $forum->user->username }}</span> <span class="mr-3">{{ $category->name }}</span></p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>


    </div>
@endsection

@section('scripts')
    @livewireScripts
@endsection
