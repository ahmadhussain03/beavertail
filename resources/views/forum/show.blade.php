@extends('layouts.app')

@section('styles')
    @livewireStyles
@endsection

@section('content')
    <livewire:forum id="{{ $id }}" />
@endsection

@section('scripts')
    @livewireScripts
@endsection
