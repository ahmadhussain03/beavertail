<div>
    <div class="container mx-auto my-2">
        <div class="mt-2 flex">
            <div class="p-8 w-full" id="recentPosts">
                <h2 class="text-2xl border-indigo-400 border-b-4 pb-3 mb-4 uppercase">{{ $forum->title }}</h2>
                <div class="flex flex-wrap justify-between items-center">
                    <div class="w-full border border-gray-500 rounded-md shadow-sm p-3">
                        <div class="flex items-center py-1">
                            <div class="mr-2">
                                <img src="{{ $forum->user->image_url }}" class="h-12 w-12 rounded-full shadow-sm" alt="">
                            </div>
                            <div class="text-gray-900">
                                <p class="text-sm">{{ $forum->user->username }}</p>
                                <p class="text-sm">{{ $forum->created_at->toFormattedDateString() }}</p>
                            </div>
                        </div>
                        <div class="py-1">
                            <p>{!! $forum->body !!}</p>
                        </div>
                        <div class="flex">
                            <div class="">
                                <p class="text-sm py-1 text-gray-700"><span class="mr-3">{{ $forum->vzt()->count() }} Views</span> <span class="mr-3">0 Comments</span> <span class="mr-3">by {{ $forum->user->username }}</span> <span class="mr-3">{{ $forum->category->name }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-4 w-full">
            <div class="px-2">
                <button wire:click="toggleForm" class="w-full uppercase bg-green-500 text-gray-100 text-2xl rounded-md shadow-md">
                    comments ({{ $comments->total() }})
                </button>

            </div>
           @if($showForm)
            @if(auth()->check() && auth()->user()->status == 1 && $forum->category->is_comment_allowed)
            <form wire:submit.prevent="createComment" class="mt-4 bg-white p-4 py-5 rounded-md shadow-lg" action="{{ route('login') }}" method="POST">
                @csrf
                <div class="">
                    <div class="">
                        <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Comment</label>
                        <input autofocus aria-label="Comment" wire:model.lazy="comment" name="comment" type="text" class="@error('comment') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Comment">
                        @error('comment')
                            <span class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="mt-6">
                    <button type="submit" class="text-white relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md bg-green-600 hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green active:bg-green-700 transition duration-150 ease-in-out">
                        Add Comment
                    </button>
                </div>
            </form>
            @endif
           @endif
            <div class="my-2">
                <ul class="flex flex-col px-2">
                    @foreach($comments as $comment)
                        <li class="bg-gray-300 p-3 rounded-md shadow-sm my-1">
                            <div class="flex items-center">
                                <img class="w-10 h-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                <div class="flex flex-col ml-2">
                                    <p class="text-sm text-gray-900">{{ $comment->user->name }}</p>
                                    <p class="text-xs text-gray-800">{{ $comment->user->username }}</p>
                                </div>
                            </div>
                            <div class="py-2 px-1">
                                <p class="text-gray-900">{{ $comment->comment }}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
                {{ $comments->links('pagination.news-pagination') }}
            </div>
        </div>
    </div>
</div>
