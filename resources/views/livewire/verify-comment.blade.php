<div>
    <div class="px-2">
        <button wire:click="toggleForm" class="min-w-full uppercase bg-green-500 text-gray-100 text-2xl rounded-md shadow-md">
            Verify ({{ $comments->total() }})
        </button>

    </div>
   @if($showForm)
    @if(auth()->check() && auth()->user()->status)
    <form wire:submit.prevent="createComment" class="mt-4 bg-white p-4 py-5 rounded-md shadow-lg" action="{{ route('login') }}" method="POST">
        @csrf
        <div class="">
            <div class="">
                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Comment</label>
                <input autofocus aria-label="Comment" wire:model.lazy="comment" name="comment" type="text" class="@error('comment') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Comment">
                @error('comment')
                    <span class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mt-2">
                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">URL</label>
                <input aria-label="URL" wire:model.lazy="url" name="url" type="text" class="@error('url') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="URL">
                @error('url')
                <span class="invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="mt-6">
            <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                Add Verify
            </button>
        </div>
    </form>
    @endif
   @endif
    <div class="my-2">
        <ul class="flex flex-col px-2">
            @foreach($comments as $comment)
                <li class="bg-gray-300 p-3 rounded-md shadow-sm my-1">
                    <div class="flex items-center">
                        <img class="w-10 h-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                        <div class="flex flex-col ml-2">
                            <p class="text-sm text-gray-900">{{ $comment->user->name }}</p>
                            <p class="text-xs text-gray-800">{{ $comment->user->username }}</p>
                        </div>
                    </div>
                    <div class="py-2 px-1">
                        <p class="text-gray-900">{{ $comment->comment }}</p>
                    </div>
                </li>
            @endforeach
        </ul>
        {{ $comments->links('pagination.news-pagination') }}
    </div>
</div>
