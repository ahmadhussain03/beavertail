<div>
    <div class="mt-2 flex">
        <div class="p-8 w-8/12" id="recentPosts">
            <h2 class="text-3xl border-indigo-400 border-b-4 pb-3 mb-4 uppercase">Recent Post</h2>
            <div class="flex flex-wrap justify-between items-center">
                @foreach($news as $new)
                    <div class="p-2 flex flex-col min-w-full">
                        <div class="shadow-lg bg-white border border-gray-400 rounded-md overflow-hidden">
                            <a href="{{ route('news.show', ['slug' => $new->slug, 'date' => $new->date]) }}">
                                <img class="mb-2 object-cover min-w-full" src="{{ $new->image_url }}" alt="{{ $new->title }}">
                            </a>
                            <ul class="flex flex-wrap items-center p-2">
                                @foreach($new->tags as $tag)
                                    <li class="mr-1 mt-1 text-sm flex items-center rounded-md bg-indigo-200 px-2 text-indigo-800">
                                        {{ $tag->name }}
                                    </li>
                                @endforeach
                            </ul>
                            <div class="px-2 flex-1 flex-grow flex-shrink-0">
                                <a href="{{ route('news.show', ['slug' => $new->slug, 'date' => $new->date]) }}" class="text-gray-800 text-xl hover:text-indigo-600">{{ $new->title }}</a>
                            </div>
                            <div class="text-gray-800 flex items-center p-2 text-xs font-semibold">
                                <svg viewBox="0 0 20 20" fill="currentColor" class="w-3 h-3"><path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd"></path></svg>
                                <span class="pl-1">{{ $new->location }}</span>
                            </div>
                            <div class="px-2 py-2 bg-gray-200 flex items-center justify-between">
                                <div class="flex items-center">
                                    <img class="h-10 w-10 rounded-full object-cover" src="{{ $new->user->image_url }}" alt="">
                                    <div class="flex flex-col pl-3">
                                        <span class="text-gray-800 text-sm font-semibold">{{ $new->user->name }}</span>
                                        <span class="text-gray-800 text-xs font-normal">{{ $new->user->username }}</span>
                                    </div>
                                </div>
                                <div class="text-gray-800 flex items-center">
                                    <svg viewBox="0 0 20 20" fill="currentColor" class="clock w-4 h-4"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z" clip-rule="evenodd"></path></svg>
                                    <span class="text-xs pl-1 font-semibold">{{ $new->date->toFormattedDateString() }} {{ $new->date->toTimeString() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="p-8 w-4/12">
            <h2 class="text-3xl border-indigo-400 border-b-4 pb-3 mb-2 uppercase">Popular Posts</h2>
            @foreach($popular as $new)
                <div class="p-2 flex flex-col">
                    <div class="shadow-lg bg-white border border-gray-400 rounded-md overflow-hidden">
                        <a href="{{ route('news.show', ['slug' => $new->slug, 'date' => $new->date]) }}">
                            <img class="mb-2 object-cover" src="{{ $new->image_url }}" alt="{{ $new->title }}">
                        </a>
                        <ul class="flex flex-wrap items-center p-2">
                            @foreach($new->tags as $tag)
                                <li class="mr-1 mt-1 text-sm flex items-center rounded-md bg-indigo-200 px-2 text-indigo-800">
                                    {{ $tag->name }}
                                </li>
                            @endforeach
                        </ul>
                        <div class="px-2 flex-1 flex-grow flex-shrink-0">
                            <a href="{{ route('news.show', ['slug' => $new->slug, 'date' => $new->date]) }}" class="text-gray-800 text-md hover:text-indigo-600">{{ $new->title }}</a>
                        </div>
                        <div class="text-gray-800 flex items-center p-2 text-xs font-semibold">
                            <svg viewBox="0 0 20 20" fill="currentColor" class="w-3 h-3"><path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd"></path></svg>
                            <span class="pl-1">{{ $new->location }}</span>
                        </div>
                        <div class="px-2 py-2 bg-gray-200 flex items-center justify-between">
                            <div class="flex items-center">
                                <img class="h-10 w-10 rounded-full object-cover" src="{{ $new->user->image_url }}" alt="">
                                <div class="flex flex-col pl-3">
                                    <span class="text-gray-800 text-sm font-semibold">{{ $new->user->name }}</span>
                                    <span class="text-gray-800 text-xs font-normal">{{ $new->user->username }}</span>
                                </div>
                            </div>
                            <div class="text-gray-800 flex items-center">
                                <svg viewBox="0 0 20 20" fill="currentColor" class="clock w-4 h-4"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z" clip-rule="evenodd"></path></svg>
                                <span class="text-xs pl-1 font-semibold">{{ $new->date->toFormattedDateString() }} {{ $new->date->toTimeString() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <input type="text" wire:model="email">
    {{ $news->links('pagination.news-pagination') }}
</div>
