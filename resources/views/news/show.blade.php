@extends('layouts.app')

@section('styles')
    @livewireStyles
@endsection

@section('content')
    <div class="container mx-auto">
        <div class="my-5">
            <div class="flex justify-center flex-col md:flex-row-reverse">
                <div class="md:flex-1">
                    <img src="{{ $news->image_url }}" alt="" class="rounded-md shadow-md">
                </div>
                <div class="md:flex-1 px-2">
                    <p class="text-gray-900 text-4xl">
                        {{ $news->title }}
                    </p>
                    <ul class="flex flex-wrap items-center pb-2">
                        @foreach($news->tags as $tag)
                            <li class="mr-1 mt-1 text-sm flex items-center rounded-md bg-indigo-200 px-2 text-indigo-800">
                                {{ $tag->name }}
                            </li>
                        @endforeach
                    </ul>
                    <p class="text-gray-900 text-lg">
                        {{ $news->synopsis }}
                    </p>
                    <p class="text-gray-900 text-sm mt-2">
                        {{ $news->location }}
                    </p>
                </div>
            </div>
        </div>
        <div class="flex">
            <div class="flex-1">
                <livewire:verify-comment :news="$news" />
            </div>
            <div class="flex-1">
                <livewire:refute-comment :news="$news" />
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @livewireScripts
@endsection
