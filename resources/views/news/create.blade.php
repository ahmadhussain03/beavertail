@extends('layouts.app')

@section('styles')
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<style>
.mapboxgl-popup {
    max-width: 200px;
}

.spinner {
  animation: rotate 2s linear infinite;
  z-index: 2;
  position: absolute;
  right: 0.5rem;
  top: 51%;
  opacity: 0.7;

  & .path {
    stroke: hsl(210, 70, 75);
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }

}

@keyframes rotate {
  100% {
    transform: rotate(360deg);
  }
}

@keyframes dash {
  0% {
    stroke-dasharray: 1, 150;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 90, 150;
    stroke-dashoffset: -35;
  }
  100% {
    stroke-dasharray: 90, 150;
    stroke-dashoffset: -124;
  }
}

</style>
@endsection

@section('content')
    <div class="container mx-auto">
        <form class="mt-8 bg-white p-8 rounded-md shadow-lg" enctype="multipart/form-data" action="{{ route('news.store') }}" method="POST">
            <h2 class="text-2xl">Submit News</h2>
            @csrf
            <div class="flex justify-between mt-2">
                <div class="flex-1 px-1">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Title</label>
                    <input aria-label="title" id="titleInput" placeholder="Enter News Title" value="{{ old('title') }}" name="title" type="text" class="{{ $errors->has('title') || $errors->has('title') ? ' is-invalid' : '' }} border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" >
                    @error('title')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="flex-1 px-1">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">URL</label>
                    <input aria-label="url" name="url" type="text" class="@error('url') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Enter News URL">
                    @error('url')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="px-1 mt-2">
                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Synopsis</label>
                <textarea rows="6" aria-label="synopsis" name="synopsis" type="text" class="@error('synopsis') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Enter News Synopsis">{{ old('synopsis') }}</textarea>
                @error('synopsis')
                    <span class="invalid-feedback">{{ $message }}</span>
                @enderror
            </div>
            <div class="flex justify-between mt-2">
                <div class="flex-1 px-1">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Date</label>
                    <input aria-label="Date" placeholder="Enter News Date" value="{{ old('date') }}" name="date" type="datetime-local" class="{{ $errors->has('date') || $errors->has('date') ? ' is-invalid' : '' }} border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" >
                    @error('date')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="flex-1 px-1 relative">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Location</label>
                    <input aria-label="location" autocomplete="off" id="locationField" value="{{ old('location') }}" type="text" class="@error('location') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Enter News Location">
                    @error('location')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                    @include('includes.loader')
                    <input type="hidden" name="lat" id="lat" value="{{ old('lat') }}" >
                    <input type="hidden" name="long" id="long" value="{{ old('long') }}">
                    <input type="hidden" name="location" id="locationInput" value="{{ old('location') }}">
                    <div id="locations" class="absolute hidden text-sm font-semibold bg-gray-200 py-1 px-2 text-gray-800 rounded-md shadow-md right-0 bottom-auto location">
                    
                    </div>
                </div>
            </div>
            <div class="flex justify-between flex-col mt-2">
                <div class="flex-1 px-1">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Tags <span class="text-xs font-light">(Enter Space to Add Tags)</span></label>
                    <input aria-label="tags" id="tagsInput" placeholder="Enter News Tags" type="text" class="@error('tags') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" >
                    @error('tags')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="flex items-center px-2 py-2" id="tags">

                </div>
            </div>
            <div class="flex justify-center items-center mt-2">
                <div class="flex justify-between items-center">
                    <input type="file" name="image" class="hidden" id="image">
                    <div class="flex flex-col">
                        <img id="imagePreview" class="h-56 w-56 object-cover object-center shadow-md overflow-hidden rounded-md border border-gray-500" src="{{ asset('images/profile/placeholder.png') }}" alt="">
                        @error('image')
                            <span class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="px-2 flex flex-col justify-between items-center">
                        <button id="selectFile" class="w-full px-2 py-2 text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">Upload Image</button>
                        <button id="removeImage" class="hidden w-full mt-2 px-2 py-2 text-sm leading-5 font-medium rounded-md bg-white border border-indigo-600 text-indigo-600 hover:text-white hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">Remove Image</button>
                    </div>
                </div>
            </div>
            <div class="flex justify-center items-center mt-4">
                <div class="flex-1 rounded-md shadow-lg overflow-hidden" style="height: 500px;">
                    <div id='map' class="w-full h-full"> </div>
                </div>
            </div>

            <div class="mt-4">
                <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <script src="{{ asset('js/create-news.js') }}"></script>
@endsection
