@extends('layouts.app')

@section('content')
<div class="container flex justify-center items-center">
    <div class="p-8 bg-white shadow-lg rounded-md mt-5 w-1/2">
        <h2>Dashboard</h2>
        @if (session('status'))
            <div class="px-8 py-3 bg-green-300 text-green-800 rounded-sm font-semibold mb-2">
                <p>{{ session('status') }}</p>
            </div>
        @endif
    </div>
</div>
@endsection
