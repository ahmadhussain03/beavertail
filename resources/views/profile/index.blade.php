@extends('layouts.app')

@section('styles')
    @livewireStyles
@endsection

@section('content')
    <div class="container mx-auto">
        <div class="mt-3">
            @if(session('success'))
                <div class="my-3 bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
                    <div class="flex items-center">
                        <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                        <div>
                            <p class="font-bold">{{ session('success') }}</p>
                        </div>
                    </div>
                </div>
            @endif
            <h2 class="text-3xl border-indigo-400 border-b-4 pb-3 mb-4 uppercase">Profile Settings</h2>
            <div class="flex">
                <div class="flex-1">
                    <div class="flex justify-center items-center">
                        <img id="image" class="rounded-md shadow-md h-64 w-64 object-center" src="{{ auth()->user()->image_url }}" alt="{{ auth()->user()->username }}">
                        <div class="flex flex-col items-center justify-center ml-2">
                            <button id="selectFile" class="w-full px-2 py-2 text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">Upload Image</button>
                            <button id="updateImage" class="hidden w-full mt-2 px-2 py-2 text-sm leading-5 font-medium rounded-md bg-white border border-indigo-600 text-indigo-600 hover:text-white hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">Update</button>
                            <button id="removeImage" class="hidden w-full mt-2 px-2 py-2 text-sm leading-5 font-medium rounded-md bg-white border border-indigo-600 text-indigo-600 hover:text-white hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">Remove Image</button>
                        </div>
                        <form id="imageForm" action="{{ route('profile.image.update') }}" class="hidden" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="file" id="fileInput" name="image">
                        </form>
                    </div>
                </div>
                <div class="flex-1">
                    <form class="bg-white rounded-md shadow-sm" action="{{ route('profile.update') }}" method="POST">
                        @csrf
                        <div class="">
                            <div class="mt-2">
                                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Email</label>
                                <input aria-label="Email" value="{{ auth()->user()->email }}" name="email" type="text" class="@error('email') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Email address">
                                @error('email')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Name</label>
                                <input aria-label="name" value="{{ auth()->user()->name }}" name="name" type="text" class="@error('name') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Full Name">
                                @error('name')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-2">
                                <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Password</label>
                                <input aria-label="Password" name="password" type="password" class="@error('password') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Password">
                                @error('password')
                                    <span class="invalid-feedback">{{ $message }}</span>
                                    <br>
                                @enderror
                                <span class="text-xs text-blue-800">Leave Blank if you don't want to update password.</span>
                            </div>
                        </div>

                        <div class="mt-6">
                            <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                <span class="absolute left-0 inset-y-0 flex items-center pl-3">
                                    <svg viewBox="0 0 20 20" fill="currentColor" class="pencil w-6 h-6"><path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"></path></svg>
                                </span>
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div>
                <livewire:user-news />
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @livewireScripts
    <script src="{{ asset('js/profile-settings.js') }}"></script>
    <script>
        $(document).ready(() => {
            window.livewire.on('postAdded', postId => {
                alert('A post was added with the id of: ' + postId);
            })
        })
    </script>
@endsection
