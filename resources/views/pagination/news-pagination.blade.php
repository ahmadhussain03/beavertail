@if($paginator->hasPages())
    <div class="px-4 py-3 flex items-center justify-center sm:px-6">
        <div class="sm:flex-1 sm:flex items-center justify-center">
            <div>
                <nav class="relative z-0 inline-flex shadow-sm">
                {{-- Previous Page Link --}}

                <button wire:click="previousPage" @if($paginator->onFirstPage()) disabled @endif  class="relative @if($paginator->onFirstPage()) cursor-not-allowed @else cursor-pointer @endif inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150" aria-label="Previous">
                    <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                </button>

                @foreach($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if(is_string($element))
                        <span class="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700">
                            ...
                        </span>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <a wire:click="gotoPage({{ $page }})" class="-ml-px relative inline-flex items-center px-4 py-2 border text-sm leading-5 font-medium focus:z-10 outline-none border-blue-200 shadow-outline-blue bg-blue-200 text-blue-700 transition ease-in-out duration-150">
                                    {{ $page }}
                                </a>
                            @else
                                <a wire:click="gotoPage({{ $page }})" class="-ml-px cursor-pointer relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                    {{ $page }}
                                </a>
                            @endif
                        @endforeach
                    @endif

                @endforeach
                {{-- Next Page Link --}}

                <button @if(!$paginator->hasMorePages()) disabled @endif wire:click="nextPage" class="-ml-px @if($paginator->hasMorePages()) cursor-pointer @else cursor-not-allowed @endif relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150" aria-label="Next">
                    <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                </button>
                </nav>
            </div>
        </div>
    </div>
@endif
