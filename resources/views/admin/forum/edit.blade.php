@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />
@endsection

@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Row-->
            <div class="row">
                <div class="col-12">
                    <!--begin::Forms Widget 2-->
                    <div class="card card-custom bg-light card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bold text-dark">Edit Forum</h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2">
                            <!--begin::Form-->
                            <form class="form" id="kt_form_1" action="{{ route('admin.forum.update', ['id' => $forum->id]) }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{ $forum->title }}" name="title" placeholder="Forum Title" />
                                    @error('title')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <select name="category" class="form-control @error('category') is-invalid @enderror" name="title" placeholder="Forum Title" >
                                        <option value="">-- Select Category --</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" @if( $forum->forum_category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <textarea id="kt_summernote_1" class="form-control summernote @error('description') is-invalid @enderror" name="body" rows="4" placeholder="Description">{{ $forum->body }}</textarea>
                                    @error('description')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mt-10">
                                    <button class="btn btn-primary font-weight-bold">Update</button>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Forms Widget 2-->
                </div>
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('scripts')
    {{-- <script src="{{ asset('assets/js/pages/crud/forms/editors/summernote.js?v=7.0.3') }}"></script> --}}
    <script>
        var KTSummernoteDemo = function () {
            // Private functions
            var demos = function () {
                $('.summernote').summernote({
                    height: 400,
                    tabsize: 2,
                    followingToolbar: true,
                    placeholder: "Forum Description",
                    fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Roboto'],
                    fontNamesIgnoreCheck: ['Roboto']
                });

                $(".summernote").summernote('fontName', 'Roboto')
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        // Initialization
        jQuery(document).ready(function() {
            KTSummernoteDemo.init();
        });
    </script>
@endsection
