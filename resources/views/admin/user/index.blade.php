@extends('layouts.admin')

@section('styles')
<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css?v=7.0.3') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">All Users</h3>
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script>
        var KTDatatablesDataSourceAjaxServer = function() {

            var initTable1 = function() {
                var table = $('#kt_datatable');

                // begin first table
                table.DataTable({
                    responsive: true,
                    searchDelay: 500,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("admin.user.list") }}',
                        type: 'GET',
                    },
                    columns: [
                        {data: 'id'},
                        {data: 'name'},
                        {data: 'username'},
                        {data: 'email'},
                        {data: 'is_banned'},
                        {data: 'action', responsivePriority: -1},
                    ],
                    columnDefs: [
                        {
                            targets: -1,
                            title: 'Actions',
                            orderable: false,
                            render: function(data, type, full, meta) {
                                
                                if(full.is_banned == 1){
                                    return '\
                                    <div class="dropdown dropdown-inline">\
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">\
                                            <i class="la la-cog"></i>\
                                        </a>\
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                                            <ul class="nav nav-hoverable flex-column">\
                                                <li class="nav-item"><a class="nav-link" href="/admin/user/'+ full.id +'/unban"><i class="nav-icon la la-edit"></i><span class="nav-text">Unban User</span></a></li>\
                                                <li class="nav-item"><a class="nav-link" href="/admin/user/'+ full.id +'/delete"><i class="nav-icon la la-trash"></i><span class="nav-text">Delete User</span></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                ';
                                } else {
                                    return '\
                                        <div class="dropdown dropdown-inline">\
                                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">\
                                                <i class="la la-cog"></i>\
                                            </a>\
                                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                                                <ul class="nav nav-hoverable flex-column">\
                                                    <li class="nav-item"><a class="nav-link" href="/admin/user/'+ full.id +'/ban"><i class="nav-icon la la-edit"></i><span class="nav-text">Ban User</span></a></li>\
                                                    <li class="nav-item"><a class="nav-link" href="/admin/user/'+ full.id +'/delete"><i class="nav-icon la la-trash"></i><span class="nav-text">Delete User</span></a></li>\
                                                </ul>\
                                            </div>\
                                        </div>\
                                    ';
                                }
                            },
                        },
                        {
                            width: '75px',
                            targets: -2,
                            render: function(data, type, full, meta) {
                                var status = {
                                    0: {'title': 'Active', 'class': ' label-light-success'},
                                    1: {'title': 'Banned', 'class': ' label-light-danger'},
                                };
                                if (typeof status[data] === 'undefined') {
                                    return data;
                                }
                                return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                            },
                        },
                    ],
                });
            };

            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            KTDatatablesDataSourceAjaxServer.init();
        });

    </script>
@endsection
