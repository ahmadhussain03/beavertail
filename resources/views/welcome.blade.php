@extends('layouts.app')

@section('styles')
    @livewireStyles
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('content')
    <div class="container mx-auto">
        @if(session('success'))
            <div class="mt-3 bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
                <div class="flex">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                <div>
                    <p class="font-bold">{{ session('success') }}</p>
                    <p class="text-sm">You can view your submitted news in profile section.</p>
                </div>
                </div>
            </div>
        @endif
        <div class="w-full my-3">
            <div class="flex-1 rounded-md shadow-lg overflow-hidden" style="height: 400px;">
                <div id='map' class="w-full h-full"> </div>
            </div>
        </div>
        @livewire('news')
    </div>
@endsection

@section('scripts')
    @livewireScripts
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <script src="{{ asset('js/show-pins.js') }}"></script>
    <script>
        document.addEventListener("livewire:load", function(event) {
            window.livewire.on('scrollTop', () => {
                $('html, body').animate({
                    scrollTop: $("#recentPosts").offset().top
                }, 800);
            });
    });
    </script>
@endsection
