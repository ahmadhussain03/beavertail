@extends('layouts.app')

@section('content')
<div class="flex items-center justify-center bg-gray-50 py-8 px-4 sm:px-6 lg:px-8">
    <div class="max-w-2xl w-full">
        <div>
            <img class="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-on-white.svg" alt="Workflow">
            <h2 class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
                Sign up to new account
            </h2>
        </div>
        <form class="mt-8 bg-white p-8 py-10 rounded-md shadow-lg" action="{{ route('register') }}" method="POST">
            @csrf
            <div class="">
                <div class="">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Username</label>
                    <input aria-label="Username" value="{{ old('username') }}" name="username" type="text" class="@error('username') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Username">
                    @error('username')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-2">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Email</label>
                    <input aria-label="Email" value="{{ old('email') }}" name="email" type="text" class="@error('email') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Email address">
                    @error('email')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-2">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Name</label>
                    <input aria-label="name" value="{{ old('name') }}" name="name" type="text" class="@error('name') is-invalid @enderror border border-gray-300 mt-1 appearance-none rounded-md relative block w-full px-3 py-2 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Full Name">
                    @error('name')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-2">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Password</label>
                    <input aria-label="Password" name="password" type="password" class="@error('password') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Password">
                    @error('password')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="mt-2">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Confirm Password</label>
                    <input aria-label="Confirm Password" name="password_confirmation" type="password" class="@error('password_confirmation') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Confirm Password">
                    @error('password_confirmation')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="mt-2">
                    <label class="text-gray-800 text-sm tracking-wider font-semibold" for="">Sign Up Code <span class="text-xs">(optional)</span></label>
                    <input aria-label="Sign Up Code" name="sign_up_code" type="text" class="@error('sign_up_code') is-invalid @enderror mt-1 appearance-none rounded-md relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5" placeholder="Sign Up Code">
                    @error('sign_up_code')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="mt-6 flex items-center justify-between">
                <div></div>
                <div class="text-sm leading-5">
                    <a href="{{ route('login') }}" class="font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                        Already Have Account?
                    </a>
                </div>
            </div>

            <div class="mt-6">
                <button type="submit" class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                    Sign Up
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
